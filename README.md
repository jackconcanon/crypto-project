# Crypto Project


## Getting started

This application connects to Binance and retrieves the BTC/USDT spot symbol ticker, this ticker information contains the best bid and ask prices as well as the top level volumes. It contains an order interface for sending orders, currently we send an order for a size of 1 BTC with a price you provide. Buy and sell orders are available and a buy and sell order must have been placed for a trade to occur.

To run the application run the main function in `cmd/algotrader.go`, e.g. `go run cmd/algotrader.go`.

## Visualisation

A web based visualisation tool has been included and is run automatically at start up, navigate to `http://localhost:8080` to see a live table of your most recent orders as well as a running total of your PnL (profit and loss).

## Glossary

There are a few words that you should understand the meaning of -

* Bid - this is the buy price in a market, bid price is lower than the ask price
* Ask - this is the sell price in a market, ask price is higher than the bid price
* Crossing - when the bid price and the ask price are the same the market is crossed and a trade occurs
* Symbol - a symbol represents a single tradable instrument, it generally represets trading between two different assets, for example BTC-USDT represents a Bitcoin to US dollars symbol. By using this symbol you can exchange Bitcoin for US dollars or vice versa
* Price - usually the dollar price of your tradable asset, for example we refer to 1 Bitcoin as $35,000
* Size - also called volume or quantity, this represetns the amount of the tradable asset you are trying to buy for your price. For example you don't have to buy a single whole Bitcoin, you can buy a portion of a Bitcoin for a lower price.
* Order - an order represents an intention to buy or sell a tradable asset, an order usually has a price, a size and a side representing a buy or a sell


## Your Task

Your task is to complete the various suggested strategies and technical indicators found in `strategy/strategy.go`. Alternatively you may use the price feed and any ideas you can come up with to send orders.

Your strategy must use prices to generate buy and sell orders, ideally you want to buy when the price is low and sell when the price is high. Financial markets constantly move between up ticks and down ticks, with different trends visible at different time resolutions. It is up to you to experiment with the technical indicators and the parameters to them to maximize your PnL.

Set the strategy you wish to use via the callback, for example the midpoint strategy -

`strategy.feedHandler = price.NewFeedHandler(BINANCE_SPOT_URL, BINANCE_SYMBOL, strategy.MidPoint, logger)`

### Sending an Order

To send an order use the `orderManager` object available in the strategy. For example -

* To send a buy order: `s.orderManager.AddOrder(order.NewBuyOrder(buyPrice))`
* To send a sell order: `s.orderManager.AddOrder(order.NewSellOrder(sellPrice))`

You must send a buy and a sell order to get a trade. In this scenario we are always buying or selling a whole Bitcoin, in real life you would likely trade smaller sizes than this.

As the order manager only stores a single order you should use this information to check if a new order is better or worse than the existing order. Remember your goal is to buy low and sell high.

### Mid Point

Mid point is the central point between two prices, in this case the bid and ask price.

The formula for mid point is -

`midPoint = (highest + lowest) / 2`

### Simple Moving Average (SMA)

A simple moving average takes prices over times and averages them to smooth out spikes in the price. It's up to you to decide what a good time period is for a moving average. 

Calculate the moving average of the mid point and use this to trigger buy and sell orders, ideally you would be able to buy when the price hits it's lowest point and sell when it hits the highest point.

The formula for a simple moving average is -

`SMA = (A1 + A2 + ... + An) / n`

### Exponential Moving Average (EMA)

An exponential moving average is very similar to the simple moving average but weights newer events by time, more recent events have a heavier impact on the moving average, the code for an EMA is shown below -

``` 
func ExponentialMovingAverage(price float64, previousEMA float64, periodTimeSeconds float64) float64 {
    deltaTimeSeconds := 1.0
    alpha := 1.0 - math.Exp(-deltaTimeSeconds/periodTimeSeconds)
    r := alpha*price + (1.0-alpha)*previousEMA
    return r
}
```

We can use the EMA to both smooth out spiky market data as well as quickly react to near term volatility. You should experiment with various time periods, increasing the time period will give a smoother curve whereas a shorter time period will give a more responsive curve.

### Moving Average Convergence Divergence (MACD)

A more complicated technical indicator that requires two exponential moving averages, these moving averages combine a longer moving average (the EMA slow) with a faster moving average (the EMA fast), by subtracting the slow from the fast we can generate a buy or sell signal. If the lines are moving apart (diverging) then the faster moving average is moving away from the slower moving average which means a good time to buy; if the lines are moving together (converging) it indicates the faster moving average is reverting back to the slower moving average, this is an indicator to sell.

Your slower moving average should combine more data points than your faster moving average.

To summarise, if the MACD is > 0 then buy, else sell.

Subtract the two to get the MACD:
`MACD = EMA_fast - EMA_slow`
