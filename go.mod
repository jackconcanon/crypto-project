module odum-research/crypto-project

go 1.21.1

require (
	github.com/gorilla/websocket v1.5.0
	go.uber.org/zap v1.26.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/stretchr/testify v1.8.4
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/sync v0.4.0
)
