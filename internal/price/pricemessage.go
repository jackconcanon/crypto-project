package price

type PriceMessage struct {
	Symbol    string  `json:"s"`
	UpdateId  int64   `json:"u"`
	BidPrice  float64 `json:"b,string"`
	BidVolume float64 `json:"B,string"`
	AskPrice  float64 `json:"a,string"`
	AskVolume float64 `json:"A,string"`
}
