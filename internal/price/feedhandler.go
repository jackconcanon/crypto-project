package price

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

type PriceMessageCallback func(recvTime int64, message *PriceMessage)

type FeedHandler struct {
	url           string
	symbol        string
	priceCallback PriceMessageCallback
	conn          *websocket.Conn
	logger        *zap.Logger
}

func NewFeedHandler(url string, symbol string, priceCallback PriceMessageCallback, logger *zap.Logger) *FeedHandler {
	return &FeedHandler{
		url:           url,
		priceCallback: priceCallback,
		logger:        logger,
		symbol:        symbol,
	}
}

// Connects to the given URL and subscribes with the given symbol.
// This blocking function then starts the listening loop.
func (f *FeedHandler) Start(ctx context.Context) error {

	if err := f.connect(ctx); err != nil {
		return err
	}

	return f.listen(ctx)
}

// Closes the websocket connection
func (f *FeedHandler) Close() {
	if f.conn != nil {
		f.conn.Close()
	}
}

func (f *FeedHandler) connect(ctx context.Context) error {
	retryCount := 1
	url := f.getUrl()
	for {
		select {
		case <-ctx.Done():
			return fmt.Errorf("connection cancelled")
		default:
		}

		if c, _, err := websocket.DefaultDialer.DialContext(ctx, url, nil); err != nil {
			if errors.Is(err, context.Canceled) {
				f.logger.Info("context cancelled while connecting to websocket, closing")
				return nil
			}
			f.logger.Error("unable to connect to websocket", zap.Error(err), zap.String("url", url), zap.Int("attempt", retryCount))
			time.Sleep(1 * time.Second)
		} else {
			f.logger.Info("connected to websocket", zap.String("url", url))
			f.conn = c
			return nil
		}
	}
}

func (f *FeedHandler) getUrl() string {
	return fmt.Sprintf("%s/ws/%s@bookTicker", f.url, f.symbol)
}

func (f *FeedHandler) listen(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			f.logger.Info("shutting down websocket listener")
			return nil
		default:
		}

		if _, message, err := f.conn.ReadMessage(); err != nil {
			return fmt.Errorf("error reading from websocket: %s", err)
		} else {
			priceMessage := &PriceMessage{}
			if err := json.Unmarshal(message, &priceMessage); err != nil {
				f.logger.Error("error unmarshalling websocket data", zap.Error(err))
				continue
			}
			f.priceCallback(time.Now().UnixNano(), priceMessage)
		}
	}
}
