package strategy

import (
	"context"
	"odum-research/crypto-project/internal/order"
	"odum-research/crypto-project/internal/plotting"
	"odum-research/crypto-project/internal/price"

	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
)

const BINANCE_SPOT_URL = "wss://stream.binance.com:9443"
const BINANCE_SYMBOL = "btcusdt"

type Strategy struct {
	orderHandler *order.OrderHandler
	feedHandler  *price.FeedHandler
	plotter      *plotting.Plotter
	logger       *zap.Logger
}

func NewStrategy(logger *zap.Logger) *Strategy {

	strategy := &Strategy{}
	strategy.feedHandler = price.NewFeedHandler(BINANCE_SPOT_URL, BINANCE_SYMBOL, strategy.MidPoint, logger)
	strategy.plotter = plotting.NewPlotter(20, logger)
	strategy.orderHandler = order.NewOrderHandler(strategy.plotter, logger)
	strategy.logger = logger

	return strategy
}

func (s *Strategy) Start(ctx context.Context) error {
	wg, groupCtx := errgroup.WithContext(ctx)

	wg.Go(func() error {
		return s.feedHandler.Start(groupCtx)
	})

	wg.Go(func() error {
		return s.plotter.Start(groupCtx)
	})

	return wg.Wait()
}

func (s *Strategy) MidPoint(recvTime int64, message *price.PriceMessage) {

}

func (s *Strategy) SimpleMovingAverage(recvTime int64, message *price.PriceMessage) {

}

func (s *Strategy) ExponentialMovingAverage(recvTime int64, message *price.PriceMessage) {

}

func (s *Strategy) MACD(recvTime int64, message *price.PriceMessage) {

}
