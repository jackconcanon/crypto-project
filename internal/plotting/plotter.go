package plotting

import (
	"context"
	"embed"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"go.uber.org/zap"
)

var (
	//go:embed chart.js index.html
	staticFS embed.FS
)

type TradeResult struct {
	Time     time.Time `json:"Time"`
	BidPrice float64   `json:"BidPrice"`
	AskPrice float64   `json:"AskPrice"`
	PnL      float64   `json:"PnL"`
}

type Table struct {
	Data         []TradeResult `json:"data"`
	TotalPnL     float64       `json:"totalPnL"`
	NotionalPerc float64       `json:"notionalPerc"`
}

type Plotter struct {
	tradeEvents  []TradeResult
	totalPnL     float64
	windowLength int64
	pos          int64
	notionalPerc float64
	logger       *zap.Logger
}

func NewPlotter(windowLength int64, logger *zap.Logger) *Plotter {
	return &Plotter{
		tradeEvents:  make([]TradeResult, 0),
		windowLength: windowLength,
		logger:       logger,
	}
}

func (p *Plotter) AddTrade(timestamp int64, bidPrice float64, askPrice float64, pnl float64, notionalPerc float64) {
	if p.pos == p.windowLength-1 {
		p.tradeEvents = p.tradeEvents[1:]
	} else {
		p.pos += 1
	}

	event := TradeResult{
		Time:     time.Unix(0, timestamp),
		BidPrice: bidPrice,
		AskPrice: askPrice,
		PnL:      pnl,
	}

	p.totalPnL += pnl
	p.tradeEvents = append(p.tradeEvents, event)
	p.notionalPerc = notionalPerc
}

func (p *Plotter) tableJSON(w io.Writer) error {

	reply := Table{
		TotalPnL:     p.totalPnL,
		NotionalPerc: p.notionalPerc,
	}

	for i := len(p.tradeEvents) - 1; i >= 0; i-- {
		reply.Data = append(reply.Data, p.tradeEvents[i])
	}

	return json.NewEncoder(w).Encode(reply)
}

func (p *Plotter) dataHandler(w http.ResponseWriter, r *http.Request) {
	if err := p.tableJSON(w); err != nil {
		p.logger.Error("error creating table data", zap.Error(err))
	}
}

func (p *Plotter) Start(ctx context.Context) error {
	server := &http.Server{
		Addr: ":8080",
	}

	http.Handle("/", http.FileServer(http.FS(staticFS)))
	http.HandleFunc("/data", p.dataHandler)

	go func() {
		p.logger.Info("serving plotter", zap.String("listenAddr", server.Addr))
		if err := server.ListenAndServe(); err != nil {
			p.logger.Info("shutting down")
		}
	}()

	<-ctx.Done()
	if err := server.Shutdown(ctx); err != nil {
		p.logger.Error("error shutting down web server")
	}
	return nil
}
