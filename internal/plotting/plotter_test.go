package plotting

import (
	"bytes"
	"odum-research/crypto-project/internal/logging"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestPlotter(t *testing.T) {
	logger := logging.GetLogger("testLogger")
	plotter := NewPlotter(5, logger)

	plotter.AddTrade(time.Now().UnixNano(), 1000, 1001, 1, 0.1)
	assert.Len(t, plotter.tradeEvents, 1)

	buf := new(bytes.Buffer)
	err := plotter.tableJSON(buf)
	assert.NoError(t, err)
	assert.NotEmpty(t, buf.Bytes())
}
