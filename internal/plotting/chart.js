async function updateChart(grid) {
    // const resp = await fetch("./data.json");
    let resp = await fetch('/data');
    let reply = await resp.json();
    grid.api.setRowData(reply.data)
    grid.api.sizeColumnsToFit()

    pnlElement = document.getElementById('pnl')
    if (reply.totalPnL > 0) {
        pnlElement.className = 'green'
    } else {
        pnlElement.className = 'red'
    }
    pnlElement.innerText = "PnL $" + reply.totalPnL.toFixed(2)

    document.getElementById('notionalPerc').innerText = 'Notional Percentage ' + reply.notionalPerc.toFixed(4) + '%'
}

document.addEventListener('DOMContentLoaded', () => {
    const columnDefs = [
        { field: "Time"},
        { field: "BidPrice" },
        { field: "AskPrice" },
        { field: "PnL"}
    ];

    // let the grid know which columns and what data to use
    const gridOptions = {
        columnDefs: columnDefs,
        rowData: []
    };

    const gridDiv = document.getElementById('table');
    new agGrid.Grid(gridDiv, gridOptions);

    setInterval(function () {
        updateChart(gridOptions)
    }, 1000);
});