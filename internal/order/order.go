package order

type Side int

const (
	Buy Side = iota
	Sell
)

type Order struct {
	price float64
	side  Side
}

func NewBuyOrder(price float64) *Order {
	return &Order{
		price: price,
		side:  Buy,
	}
}

func NewSellOrder(price float64) *Order {
	return &Order{
		price: price,
		side:  Sell,
	}
}
