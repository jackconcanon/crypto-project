package order

import (
	"odum-research/crypto-project/internal/plotting"
	"time"

	"go.uber.org/zap"
)

type OrderHandler struct {
	BuyOrder  *Order
	SellOrder *Order
	Profit    float64
	Notional  float64
	plotter   *plotting.Plotter
	logger    *zap.Logger
}

func NewOrderHandler(plotter *plotting.Plotter, logger *zap.Logger) *OrderHandler {
	return &OrderHandler{
		plotter: plotter,
		logger:  logger,
	}
}

func (p *OrderHandler) getNotionalPercentage() float64 {
	return (p.Profit / p.Notional) * 100.0
}

func (p *OrderHandler) AddOrder(order *Order) {
	if order.side == Buy {
		p.BuyOrder = order
	} else {
		p.SellOrder = order
	}
	p.match()
}

func (p *OrderHandler) HasBuyOrder() bool {
	return p.BuyOrder != nil
}

func (p *OrderHandler) HasSellOrder() bool {
	return p.SellOrder != nil
}

func (p *OrderHandler) ClearOrders() {
	p.BuyOrder = nil
	p.SellOrder = nil
}

func (p *OrderHandler) match() {
	if p.BuyOrder != nil && p.SellOrder != nil {
		pnl := p.SellOrder.price - p.BuyOrder.price
		p.Profit += pnl
		p.Notional += p.SellOrder.price + p.BuyOrder.price
		p.plotter.AddTrade(time.Now().UnixNano(), p.BuyOrder.price, p.SellOrder.price, pnl, p.getNotionalPercentage())
		p.logger.Info("executed order", zap.Float64("totalPnL", p.Profit), zap.Float64("notional", p.Notional), zap.Float64("notionalPerc", p.getNotionalPercentage()))
		p.ClearOrders()
	}
}
