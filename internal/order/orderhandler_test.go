package order

import (
	"odum-research/crypto-project/internal/logging"
	"odum-research/crypto-project/internal/plotting"

	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMatchSimple(t *testing.T) {
	logging := logging.GetLogger("testLogger")
	plotter := plotting.NewPlotter(10, logging)
	handler := NewOrderHandler(plotter, logging)

	// buy order
	buyOrder := NewBuyOrder(100)
	handler.AddOrder(buyOrder)
	assert.Equal(t, 0.0, handler.Profit)
	assert.NotNil(t, handler.BuyOrder)
	assert.Nil(t, handler.SellOrder)

	//  add a sell order
	sellOrder := NewSellOrder(101)
	handler.AddOrder(sellOrder)
	assert.Equal(t, 1.0, handler.Profit)
	assert.Equal(t, 201.0, handler.Notional)
	assert.Nil(t, handler.BuyOrder)
	assert.Nil(t, handler.SellOrder)
}
