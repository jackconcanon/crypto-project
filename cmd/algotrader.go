package main

import (
	"context"
	"odum-research/crypto-project/internal/logging"
	"odum-research/crypto-project/internal/strategy"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"
)

func main() {

	logger := logging.GetLogger("algotrader")
	strategy := strategy.NewStrategy(logger)

	ctx, cancel := context.WithCancel(context.Background())
	// Set up the signal handler
	shutdownSignalsChan := make(chan os.Signal, 1)
	signal.Notify(shutdownSignalsChan, os.Interrupt, syscall.SIGTERM)
	go func() {
		for range shutdownSignalsChan {
			logger.Info("shutting down service")
			cancel()
		}
	}()

	if err := strategy.Start(ctx); err != nil {
		logger.Error("error running algo trader", zap.Error(err))
	}
}
